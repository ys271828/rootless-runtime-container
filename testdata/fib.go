package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	n := 100
	if len(os.Args) > 1 {
		var err error
		n, err = strconv.Atoi(os.Args[1])
		if err != nil || n < 1 {
			fmt.Println("argument needs to be positive integer or empty")
			os.Exit(1)
			return
		}
	}
	for i := 1; i <= n; i++ {
		fmt.Println(fibonacci(i))
	}
}

func fibonacci(n int) int {
	if n <= 1 {
		return n
	}
	return fibonacci(n-1) + fibonacci(n-2)
}
