# Rootless Runtime Container

This is a learn-by-hacking kind of code.

> "I hear and I forget. I see and I remember. I do and I understand." - Confucius

## Requirements

* Linux (tested on Ubuntu 22.04)
* cgroup v2

## Preparations

From https://rootlesscontaine.rs/getting-started/common/cgroup2/

#### Enabling CPU, CPUSET, and I/O delegation

By default, a non-root user can only get memory controller and pids controller to be delegated.

```
$ cat /sys/fs/cgroup/user.slice/user-$(id -u).slice/user@$(id -u).service/cgroup.controllers
memory pids
```

To allow delegation of other controllers such as cpu, cpuset, and io, run the following commands:

```
$ sudo mkdir -p /etc/systemd/system/user@.service.d
$ cat <<EOF | sudo tee /etc/systemd/system/user@.service.d/delegate.conf
[Service]
Delegate=cpu cpuset io memory pids
EOF
$ sudo systemctl daemon-reload
$ sudo echo '+cpuset +cpu +io +memory +pids' > /sys/fs/cgroup/user.slice/user-$(id -u).slice/user@$(id -u).service/cgroup.subtree_control 
```

**Needed after restart:**

```
$ sudo echo '+cpuset +cpu +io +memory +pids' > /sys/fs/cgroup/user.slice/user-$(id -u).slice/user@$(id -u).service/cgroup.subtree_control 
```

#### Creating proc directory

*Run this without sudo*

```
$ mkdir -p ./testdata/proc
```

## Usage

#### Start

`go run cmd/runner/main.go start <command>`

e.g.: `go run cmd/runner/main.go start ./testdata/fib`

#### Log

`go run cmd/runner/main.go log <id>`

e.g.: `go run cmd/runner/main.go log 5a83358a1e6bba63e27372a6cbca5eba`

#### Stop

`go run cmd/runner/main.go stop <id>`

e.g.: `go run cmd/runner/main.go stop 5a83358a1e6bba63e27372a6cbca5eba`