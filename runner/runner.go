package runner

import (
	"crypto/rand"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"syscall"
	"unsafe"
)

const (
	selfExe = "/proc/self/exe"
	prefix  = "my_processes"
)

var procDir string

func init() {
	wd, err := os.Getwd()
	if err != nil {
		log.Fatalf("FATAL: get working directory error: %v", err)
	}

	procDir = filepath.Join(wd, "testdata/proc")
}

func Output(jobID string) {
	log.Printf("DEBUG: outputting: %v", jobID)

	pid, err := getPID(jobID)
	if err != nil {
		log.Printf("ERROR: error getting pid: %v", err)
		return
	}

	fn := getOutFilename(jobID)

	f, _ := os.Open(fn)
	defer f.Close()

	fd, _ := syscall.InotifyInit()
	defer syscall.Close(fd)

	wd, _ := syscall.InotifyAddWatch(fd, fn, syscall.IN_MODIFY|syscall.IN_CLOSE_WRITE)
	defer syscall.InotifyRmWatch(fd, uint32(wd))

	for {
		// read & send until EOF
		buf := make([]byte, 1024)
		n, err := f.Read(buf)
		if n > 0 {
			os.Stdout.Write(buf[:n])
		}
		eof := errors.Is(err, io.EOF)
		if err != nil && !eof {
			return
		}

		if eof {
			// process is not running, stop streaming
			_, err := os.Stat(filepath.Join("/proc", strconv.Itoa(pid)))
			if err != nil {
				if !os.IsNotExist(err) {
					log.Printf("ERROR: stat error: %v", err)
				}
				return
			}

			// process is running, wait until output file is modified/closed or error
			select {
			case event := <-waitInotifyEvent(fd):
				if event != syscall.IN_MODIFY {
					return // process stopped or error
				}
			}
		}
	}
}

func waitInotifyEvent(fd int) <-chan uint32 {
	ch := make(chan uint32)
	go func() {
		defer close(ch)
		buffer := make([]byte, syscall.SizeofInotifyEvent)
		if _, err := syscall.Read(fd, buffer); err != nil {
			// log ...
			ch <- 0
			return
		}
		event := (*syscall.InotifyEvent)(unsafe.Pointer(&buffer[0]))
		ch <- event.Mask
	}()
	return ch
}

func Stop(jobID string) {
	log.Printf("DEBUG: stopping: %v", jobID)

	pid, err := getPID(jobID)
	if err != nil {
		log.Printf("ERROR: error getting pid: %v", err)
		return
	}

	if err := syscall.Kill(-pid, syscall.SIGKILL); err != nil {
		log.Printf("ERROR: killing process error: %v", err)
		return
	}

	if err := delCG(jobID); err != nil {
		log.Printf("ERROR: deleting cgroup error: %v", err)
		return
	}
}

func Start(cmdArgs []string) {
	log.Printf("DEBUG: starting: %v", cmdArgs)

	jobID, err := newJobID()
	if err != nil {
		log.Printf("ERROR: creating new job ID error: %v", err)
		return
	}

	if err := createCG(jobID); err != nil {
		log.Printf("ERROR: creating cgroup error: %v", err)
		return
	}
	if err := restrictCG(jobID, os.Getpid()); err != nil {
		log.Printf("ERROR: establishing cgroup restrictions error: %v", err)
		return
	}

	cmd := exec.Command(selfExe, append([]string{jobID}, cmdArgs...)...)
	cmd.SysProcAttr = &syscall.SysProcAttr{
		Setpgid:      true,
		Cloneflags:   syscall.CLONE_NEWNS | syscall.CLONE_NEWPID | syscall.CLONE_NEWUSER | syscall.CLONE_NEWUTS,
		Unshareflags: syscall.CLONE_NEWNS,
		UidMappings: []syscall.SysProcIDMap{
			{
				ContainerID: 0,
				HostID:      os.Getuid(),
				Size:        1,
			},
		},
		GidMappings: []syscall.SysProcIDMap{
			{
				ContainerID: 0,
				HostID:      os.Getgid(),
				Size:        1,
			},
		},
	}

	if err := cmd.Start(); err != nil {
		log.Printf("ERROR: starting command error: %v", err)
		return
	}

	fmt.Println(jobID)
}

func getRunnerErrFilename(jobID string) string {
	return filepath.Join(os.TempDir(), fmt.Sprintf("%s-runner-out-%s", prefix, jobID))
}

func getOutFilename(jobID string) string {
	return filepath.Join(os.TempDir(), fmt.Sprintf("%s-out-%s", prefix, jobID))
}

func Child(selfExeArgs []string) {
	jobID := selfExeArgs[0]

	runnerErrName := getRunnerErrFilename(jobID)
	runnerErrFile, _ := os.OpenFile(runnerErrName, os.O_CREATE|os.O_WRONLY, 0o644)
	defer runnerErrFile.Close()

	runnerLog := log.New(runnerErrFile, "", log.LstdFlags)

	runnerLog.Printf("DEBUG: child starting: %v", selfExeArgs[1:])

	defer func() {
		if err := delCG(jobID); err != nil {
			runnerLog.Printf("ERROR: deleting cgroup error: %v", err)
			return
		}
	}()

	outFileName := getOutFilename(jobID)
	outFile, err := os.OpenFile(outFileName, os.O_CREATE|os.O_WRONLY, 0o644)
	if err != nil {
		runnerLog.Printf("ERROR: creating out file %s error: %v", outFileName, err)
		return
	}
	defer outFile.Close()

	if err := syscall.Sethostname([]byte("container")); err != nil {
		runnerLog.Printf("ERROR: setting hostname error: %v", err)
		return
	}

	if err := syscall.Mount("proc", procDir, "proc", 0, ""); err != nil {
		runnerLog.Printf("ERROR: mounting proc error: %v", err)
		return
	}

	defer func() {
		if err := syscall.Unmount(procDir, 0); err != nil {
			runnerLog.Printf("ERROR: unmounting proc error: %v", err)
			return
		}
	}()

	cmd := exec.Command(selfExeArgs[1], selfExeArgs[2:]...)
	cmd.Stdout = outFile
	cmd.Stderr = outFile
	cmd.SysProcAttr = &syscall.SysProcAttr{
		Setpgid: true,
	}

	if err := cmd.Start(); err != nil {
		runnerLog.Printf("ERROR: starting child process error: %v", err)
		return
	}

	if err := cmd.Wait(); err != nil {
		runnerLog.Printf("ERROR: waiting child process error: %v", err)
		return
	}
}

// newJobID creates a jobID
func newJobID() (string, error) {
	bytes := make([]byte, 16)
	if _, err := rand.Read(bytes); err != nil {
		return "", err
	}
	return hex.EncodeToString(bytes), nil
}
