package runner

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

// It needs cgroup v2

const cgBaseTmpl = "/sys/fs/cgroup/user.slice/user-%[1]d.slice/user@%[1]d.service"

const (
	filePerm = 0o644
	dirPerm  = 0o755
)

const sysBlock = "/sys/block/"

func cgBase() string {
	return fmt.Sprintf(cgBaseTmpl, os.Getuid())
}

func cgDir(jobID string) string {
	return filepath.Join(cgBase(), jobID)
}

// get all major:minor of sd* & hd*
func majorMinors() ([]string, error) {
	entries, err := os.ReadDir(sysBlock)
	if err != nil {
		return nil, err
	}

	result := []string{}

	for _, entry := range entries {
		name := entry.Name()
		if strings.HasPrefix(name, "hd") || strings.HasPrefix(name, "sd") {
			content, err := os.ReadFile(filepath.Join(sysBlock, name, "dev"))
			if err != nil {
				return nil, err
			}
			result = append(result, strings.TrimSpace(string(content)))
		}
	}

	return result, nil
}

func restrictCG(jobID string, pid int) error {
	cgDir := cgDir(jobID)

	// hardcoded to 0.2s of 1s
	if err := os.WriteFile(filepath.Join(cgDir, "cpu.max"), []byte("200000 1000000"), filePerm); err != nil {
		return err
	}

	// hardcoded to 100mi
	if err := os.WriteFile(filepath.Join(cgDir, "memory.high"), []byte("100000000"), filePerm); err != nil {
		return err
	}

	mms, err := majorMinors()
	if err != nil {
		return err
	}

	for _, mm := range mms {
		// hardcoded to read limit at 2M BPS and write at 120 IOPS
		f, err := os.OpenFile(filepath.Join(cgDir, "io.max"), os.O_APPEND|os.O_WRONLY|os.O_CREATE, filePerm)
		if err != nil {
			return err
		}
		defer f.Close()
		if _, err = f.WriteString(fmt.Sprintf("%s rbps=2097152 wiops=120", mm)); err != nil {
			return err
		}
	}

	// restrict the process
	f, err := os.OpenFile(filepath.Join(cgDir, "cgroup.procs"), os.O_APPEND|os.O_WRONLY|os.O_CREATE, filePerm)
	if err != nil {
		return err
	}
	defer f.Close()
	if _, err = f.WriteString(strconv.Itoa(pid)); err != nil {
		return err
	}

	return nil
}

func createCG(jobID string) error {
	dir := cgDir(jobID)
	if err := os.Mkdir(dir, dirPerm); err != nil {
		return fmt.Errorf("creating %s error: %w", dir, err)
	}
	return nil
}

func delCG(jobID string) error {
	dir := cgDir(jobID)
	if err := os.Remove(dir); err != nil {
		return fmt.Errorf("deleting %s error: %w", dir, err)
	}
	return nil
}

// getPID looks up pid number based on the jobID
func getPID(jobID string) (int, error) {
	name := filepath.Join(cgDir(jobID), "cgroup.procs")
	content, err := os.ReadFile(name)
	if err != nil {
		return -1, fmt.Errorf("reading %s error: %w", name, err)
	}
	pid := -1
	pids := strings.Split(string(content), "\n")
	for _, s := range pids {
		if p, err := strconv.Atoi(s); err == nil && (pid == -1 || p < pid) {
			pid = p
		}
	}
	return pid, nil
}
