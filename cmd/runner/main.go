package main

import (
	"log"
	"lp/runner"
	"os"
)

const selfExe = "/proc/self/exe"

func main() {
	log.SetOutput(os.Stderr)

	if os.Args[0] == selfExe {
		runner.Child(os.Args[1:])
		return
	}

	switch os.Args[1] {
	case "start":
		runner.Start(os.Args[2:])
	case "stop":
		runner.Stop(os.Args[2])
	case "output", "log":
		runner.Output(os.Args[2])
	default:
		panic("help")
	}
}
